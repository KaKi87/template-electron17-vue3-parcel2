git clone --recurse-submodules https://git.kaki87.net/KaKi87/template-electron17-vue3-parcel2.git "$1"
cd "$1"
rm -r .git
git init
git add .
git commit -m ":tada: Initial commit"
yarn install
cp config.example.js config.js
bash
exit