import fs from 'fs';
import { Parcel } from '@parcel/core';
import electronBuilder from 'electron-builder';

process.env['PARCEL_WORKER_BACKEND'] = 'process';

(async () => {
    try { fs.rmSync('./dist', { recursive: true }); } catch {}
    try { fs.rmSync('./build', { recursive: true }); } catch {}
    const packageFileLines = fs.readFileSync('./package.json', 'utf8').split('\n');
    fs.writeFileSync('./package.json', [...packageFileLines.slice(0, 2), ...packageFileLines.slice(4)].join('\n'));
    await new Parcel({
        entries: 'src-vue/index.html',
        defaultConfig: '@parcel/config-default',
        env: { NODE_ENV: 'production' },
        defaultTargetOptions: { publicUrl: '.' }
    }).run();
    fs.writeFileSync('./package.json', packageFileLines.join('\n'));
    await electronBuilder.build({
        targets: {
            'linux': electronBuilder.Platform.LINUX.createTarget(),
            'win32': electronBuilder.Platform.WINDOWS.createTarget(),
            'darwin': electronBuilder.Platform.MAC.createTarget()
        }[process.platform],
        config: {
            files: fs
                .readdirSync('.', { withFileTypes: true })
                .filter(item => ![
                    '.git',
                    '.gitignore',
                    'build.mjs',
                    'config.example.js',
                    'yarn.lock',
                    ...fs
                        .readFileSync('./.gitignore', 'utf8')
                        .split('\n')
                        .filter(item => !['dist', 'node_modules', 'config.js'].includes(item))
                ].includes(item.name))
                .map(item => `${item.name}${item.isDirectory() ? '/**' : ''}`),
            directories: {
                output: 'build',
                buildResources: 'src-electron/assets'
            },
            win: {
                target: [{
                    target: 'nsis',
                    arch: 'x64'
                }],
            },
            linux: {
                target: [{
                    target: 'AppImage',
                    arch: 'x64'
                }],
            },
            mac: {
                target: [{
                    target: 'dmg'
                }]
            }
        }
    });
})().catch(console.error);