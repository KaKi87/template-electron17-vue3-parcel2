module.exports = {
    /** @type {boolean} */
    isSpellcheckEnabled: false,
    /** @type {boolean} */
    isTitleBarEnabled: true,
    /** @type {boolean} */
    isMenuBarEnabled: false,
    /** @type {boolean} */
    isCorsEnabled: false,
    /** @type {number} */
    devServerPort: 1234
};