const
    electron = require('electron'),
    {
        isSpellcheckEnabled,
        isTitleBarEnabled,
        isMenuBarEnabled,
        isCorsEnabled,
        devServerPort
    } = require('./config.js');

(async () => {
    if(typeof electron === 'string'){
        const
            { spawn } = require('node:child_process'),
            { Parcel } = require('@parcel/core');
        await new Parcel({
            entries: 'src-vue/index.html',
            defaultConfig: '@parcel/config-default',
            env: { NODE_ENV: 'development' },
            serveOptions: { port: devServerPort },
            hmrOptions: { port: devServerPort }
        }).watch();
        const child = spawn(
            electron,
            ['start.js'],
            { stdio: 'inherit' }
        );
        child.on('close', () => process.exit());
    }
    else {
        const
            path = require('node:path'),
            {
                default: installExtension,
                VUEJS3_DEVTOOLS
            } = electron.app.isPackaged ? {} : require('electron-devtools-installer');
        electron.nativeTheme.themeSource = 'dark';
        await electron.app.whenReady();
        if(!electron.app.isPackaged)
            await installExtension(VUEJS3_DEVTOOLS);
        const mainWindow = new electron.BrowserWindow({
            webPreferences: {
                preload: path.join(__dirname, './src-electron/preload.js'),
                spellcheck: isSpellcheckEnabled
            },
            icon: path.join(__dirname, './src-electron/assets/icon.png'),
            frame: isTitleBarEnabled,
            show: false
        });
        mainWindow.setMenuBarVisibility(isMenuBarEnabled);
        if(!isCorsEnabled){
            const corsHeaders = [
                'access-control-allow-origin',
                'access-control-allow-methods',
                'access-control-allow-headers'
            ];
            mainWindow.webContents.session.webRequest.onHeadersReceived((details, callback) => callback({
                responseHeaders: {
                    ...Object.fromEntries(
                        Object
                            .entries(details.responseHeaders)
                            .filter(([header]) => !corsHeaders.includes(header.toLowerCase()))
                    ),
                    ...Object.fromEntries(corsHeaders.map(header => [header, ['*']]))
                }
            }));
        }
        mainWindow.webContents.openDevTools({
            mode: 'detach',
            activate: !electron.app.isPackaged
        });
        if(electron.app.isPackaged){
            mainWindow.webContents.closeDevTools();
            await mainWindow.loadFile('./dist/index.html');
        }
        else
            await mainWindow.loadURL(`http://localhost:${devServerPort}/`);
        mainWindow.show();
    }
})().catch(console.error);